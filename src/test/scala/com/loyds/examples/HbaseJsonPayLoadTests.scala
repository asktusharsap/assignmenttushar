package com.loyds.examples


import org.apache.spark.sql.SparkSession
import org.junit.Assert._
import org.junit.Test


class HbaseJsonPayLoadTests {
  val spark = SparkSession.builder().appName("Hbase JSON PayLoad Example").master("local[*]").getOrCreate()

  val station_data="input/station_data.csv"
  val trip_data="input/trip_data.csv"

  val obj=new HbaseJsonPayLoadWrapper()
  @Test def testTripDataSchema {
    assertEquals(9,obj.readFile(trip_data,spark).columns.size)
  }

  @Test def testStationDataSchema {
    assertEquals(7,obj.readFile(station_data,spark).columns.size)
  }

  @Test def testProcess {
    val tripData=obj.readFile(trip_data,spark)
    val stationData=obj.readFile(station_data,spark)
    assertEquals(2,obj.processResult(tripData,stationData,spark).columns.size)
  }
}

package com.loyds.examples

import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions._
import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods._

case class MyCase(TripID: String, Duration: String, Bike: String, StartStation: String, EndStation: String, landmark: String)

class HbaseJsonPayLoadWrapper extends Serializable {


  val getConcatenated = udf((first: String, second: String) => {
    first + "," + second
  })

  def convertRowToJSON(row: MyCase) = {
    val json =
      ("bike" -> row.Bike) ~
        ("start_station" -> row.StartStation) ~
        ("end_station" -> row.EndStation) ~
        ("landmarks" -> row.landmark) ~
        ("total_duration" -> row.Duration)
    (row.TripID, compact(render(json)).toString)
  }

  //val spark = SparkSession.builder().appName("Hbase JSON PayLoad Example").getOrCreate()

  def readFile(fileName: String, spark: SparkSession): DataFrame = {
    val df = spark.read.format("csv").option("sep", ",").option("inferSchema", "false").option("header", "true").load(fileName)
    df
  }

  def processResult(trip_data:DataFrame,station_data:DataFrame,spark: SparkSession):DataFrame ={


    import spark.sqlContext.implicits._
    val StatStationData = trip_data.join(station_data, col("Start Terminal") === col("station_id"), "inner").withColumn("Start Station", col("name")).withColumn("StartStationlandmark", col("landmark")).drop("name").drop("Start Terminal").drop("station_id").drop("landmark")
    val FinalData = StatStationData.join(station_data, col("End Terminal") === col("station_id"), "inner").withColumn("End Station", col("name")).withColumn("Final landmark", when(col("landmark") === col("StartStationlandmark"), col("landmark")).otherwise(getConcatenated($"landmark", $"StartStationlandmark"))).drop("name").drop(("End Terminal")).drop("station_id").drop("landmark").drop("StartStationlandmark")

    val FinalDataDf = FinalData.withColumn("TripID", col("Trip ID")).withColumn("EndStation", col("End Station")).withColumn("landmark", split(col("Final landmark"), "\\,")).withColumn("Bike", col("Bike #")).withColumn("StartStation", col("Start Station")).drop("Trip ID").drop("End Station").drop("Final landmark").drop("Bike #").drop("Start Station")
    val encoderd = org.apache.spark.sql.Encoders.product[MyCase]

    val FinalDataDS = FinalDataDf.as[MyCase](encoderd)

    val JsonPlayloadData = FinalDataDS.map(convertRowToJSON)

    val newNames = Seq("p_key", "value")
    val dfRenamed = JsonPlayloadData.toDF(newNames: _*)

    dfRenamed

    }

}

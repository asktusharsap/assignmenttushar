package com.loyds.examples

import org.apache.spark.sql.SparkSession
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.execution.datasources.hbase.HBaseTableCatalog
import org.apache.spark.sql.functions.udf

  object hbase {
    def main(args: Array[String]) {
      val spark = SparkSession.builder().appName("hbase poc").getOrCreate()
      val sqlContext = spark.sqlContext

      import sqlContext.implicits._


      //create df from excel

      val cycleride_data = spark.read.format("csv").option("sep", ",").option("inferSchema", "false").option("header", "true").load("gs://loyds-assignment/cycle_ride.csv")

      val getConcatenated = udf((first: String, second: String) => {
        first + "-" + second
      })

      val cycleride = cycleride_data.withColumn("p_key", getConcatenated($"cust_id", $"record_id"))

      println("Please find the below data as read from Excel File")

      val cat =
        s"""{
           |"table":{"namespace":"default", "name":"cycleride", "tableCoder":"PrimitiveType"},
           |"rowkey":"p_key",
           |"columns":{
           |"p_key":{"cf":"rowkey", "col":"p_key", "type":"string"},
           |"cust_id":{"cf":"Data", "col":"cust_id", "type":"string"},
           |"record_id":{"cf":"Data", "col":"record_id", "type":"string"},
           |"commute":{"cf":"Data", "col":"commute", "type":"string"},
           |"distance":{"cf":"Data", "col":"distance", "type":"string"},
           |"elapsed_time":{"cf":"Data", "col":"elapsed_time", "type":"string"},
           |"kilojoules":{"cf":"Data", "col":"kilojoules", "type":"string"},
           |"moving_time":{"cf":"Data", "col":"moving_time", "type":"string"},
           |"name":{"cf":"Data", "col":"name", "type":"string"},
           |"start_date_local":{"cf":"Data", "col":"start_date_local", "type":"string"},
           |"start_date_local_tms":{"cf":"Data", "col":"start_date_local_tms", "type":"string"}
           |}
           |}""".stripMargin

      cycleride.write.options(
        Map(HBaseTableCatalog.tableCatalog -> cat, HBaseTableCatalog.newTable -> "5"))
        .format("org.apache.spark.sql.execution.datasources.hbase")
        .save()

      println("Data has been loaded in BigTable")

      val df = sqlContext
        .read
        .options(Map(HBaseTableCatalog.tableCatalog -> cat))
        .format("org.apache.spark.sql.execution.datasources.hbase")
        .load()

      println("Please find the below data from BigTable for Customer ID 99177")

      // To Print records for a particular customer_id
      df.filter("cust_id = '99177'").show()

      println("That's it folks!! Time to relax, Code Done !!")

      spark.stop()

      // TO Read For Particular Data.
      /*scan 'cycleride' ,
    {
      FILTER => "SingleColumnValueFilter('Data','cust_id',=, 'binary:99177')"
    }*/

    }

  }

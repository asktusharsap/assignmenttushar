package com.loyds.examples

import org.apache.spark.sql.{SparkSession}
import com.loyds.examples.HbaseJsonPayLoadWrapper
import org.apache.spark.sql.execution.datasources.hbase.HBaseTableCatalog

object HbaseJsonPayLoad {
  val spark = SparkSession.builder().appName("Hbase JSON PayLoad Example").getOrCreate()

  def main(args: Array[String]){


    try{
    val caller = new HbaseJsonPayLoadWrapper()

    val station = caller.readFile("gs://loyds-assignment/station_data.csv",spark )
    val station_data = station.drop("lat").drop("long").drop("dockcount").drop("installation")

    val trip = caller.readFile("gs://loyds-assignment/trip_data.csv",spark )
    val trip_data = trip.drop("Start Date").drop("End Date").drop("Subscriber Type").drop("Zip Code")


    val dfRenamed = caller.processResult(trip_data,station_data,spark)

    val cat =
      s"""{
         |"table":{"namespace":"default", "name":"tripdata", "tableCoder":"PrimitiveType"},
         |"rowkey":"p_key",
         |"columns":{
         |"p_key":{"cf":"rowkey", "col":"p_key", "type":"string"},
         |"value":{"cf":"Data", "col":"value", "type":"string"}
         |}
         |}""".stripMargin

    dfRenamed.write.options(
      Map(HBaseTableCatalog.tableCatalog -> cat, HBaseTableCatalog.newTable -> "5"))
      .format("org.apache.spark.sql.execution.datasources.hbase")
      .save()

    println("Data has been loaded in BigTable")
    }
    catch {
      case ex:Exception => println("Exception Occured : "+ex.printStackTrace())
        System.exit(1)
    }

    spark.stop()
  }
}